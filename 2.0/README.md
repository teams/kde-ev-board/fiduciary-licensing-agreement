# KDE FLA 2.0

The KDE FLA 2.0 uses the unchanged FLA text from contributoragreements.org,
and uses no special relicensing language. The FRP has been dropped.

## Usage

The KDE FLA 2.0 was introduced in 2022.

## Creation Information

For reference, here is what we filled in in the contributoragreements.org site:

Entity:         KDE e.V.
Project Name:   KDE
Project Site:   https://kde.org/
Project Email:  kde-ev-board@kde.org
Signing Site:   https://ev.kde.org/rules/fla/
Jurisdiction:   Germany

Copyright:      Option 3 (outbound license according to policy)
Policy Site:    https://community.kde.org/Policies/Licensing_Policy

[Full link](https://contributoragreements.org/ca-cla-chooser/?beneficiary-name=KDE+e.V.+&project-name=KDE&project-website=https%3A%2F%2Fkde.org%2F&project-email=kde-ev-board%40kde.org&process-url=https%3A%2F%2Fev.kde.org%2Frules%2Ffla%2F&project-jurisdiction=Germany&agreement-exclusivity=exclusive&fsfe-compliance=&fsfe-fla=&outbound-option=fsfe&outboundlist=&outboundlist-custom=&medialist=____________________&patent-option=Traditional&your-date=&your-name=&your-title=&your-address=&your-patents=&pos=apply&action=)

