# KDE FLA 1.3

The KDE FLA 1.3 series derives from FSFE's FLA 1.0. The points of
divergence between the original and KDE's version are the introduction
of the FRP (a policy to restrict how the fiduciary may act), and then
minor updates to addresses and version-control systems.

The version history (from the website) is this:

1.3.5 Unified the prefab and non-prefab document.
1.3.4 Updated post address of KDE e.V.
1.3.3 Updated post address of KDE e.V.
1.3.2 (with associated FRP version 1.3.1) Added FLA with boilerplate text for KDE SVN account holders.
1.3.1 Clarify text and remove some licenses from FRP that we don’t actually want to use.
1.3 Clarify text by referring to specific version of KDE e.V. charter.
1.2 Changes of address.
1.2 Changes in the way the document is created.
1.1 Initial version (the FSFE document is considered version 1.0).

## Usage

No versions of the KDE FLA prior to 1.3 were ever signed.

The use of KDE FLA 1.3 was deprecated in 2022. it is still possible
to sign this version, but no longer recommended.
