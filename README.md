# KDE FLA

The Fiduciary License Agreement (FLA) is a legal document that assigns
those rights that **are** assignable in a copyrighted work (e.g.
software code contributed to KDE) to a fiduciary (e.g. KDE e.V.)
that safeguards those rights.

The KDE FLA is based on the FSFE FLA (version 1) and on the FLA from 
[contributoragreements.org](https://contributoragreements.org/) (version 2).

## Version 2.0

Version 2.0 is a **new** version, to be introduced in 2022,
with modern language and a simplified relicensing policy.

## Version 1.3

This is the "legacy" version, to be phased out in 2022.
It is strongly tied to the KDE4 era. Existing signatures remain
valid and it is still possible to sign new copies of this FLA.
